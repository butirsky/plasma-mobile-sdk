set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1.1)
set(CMAKE_SYSTEM_PROCESSOR arm)

#set(CMAKE_SYSROOT /home/bam/Develop/CrossCompile/pinephone-manjaro-rootfs)
set(CMAKE_SYSROOT $ENV{HOME}/Develop/pinephone-manjaro-sysroot)
#set(CMAKE_SYSROOT /mnt)

#set(tools /home/bam/Develop/CrossCompile/aarch64-unknown-linux-gnu)
set(tools $ENV{HOME}/Develop/x-tools8/aarch64-unknown-linux-gnu)
set(CMAKE_C_COMPILER ${tools}/bin/aarch64-unknown-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER ${tools}/bin/aarch64-unknown-linux-gnu-g++)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(BUILD_TESTING OFF)
