FROM docker.io/library/archlinux

# Install dependencies
#---------------------------
# build-essential
#RUN pacman --noconfirm -Sy gcc git cmake make vim doxygen bzr
RUN pacman -Sy
RUN pacman --noconfirm --needed -S qemu-arch-extra

RUN pacman --noconfirm --needed -S cmake
#RUN pacman --noconfirm --needed -S make
RUN pacman --noconfirm --needed -S ninja

RUN pacman --noconfirm --needed -S pkgconf
RUN pacman --noconfirm --needed -S flex

RUN pacman --noconfirm --needed -S ccache

RUN pacman --noconfirm --needed -S xcb-util-wm
#RUN pacman --noconfirm --needed -S base-devel


#ENV QEMU_LD_PREFIX=~/Develop/pinephone-manjaro-sysroot

#VOLUME /root/.ccache
RUN mkdir -p /root/Develop/pinephone-manjaro-sysroot && ln -s usr/lib /root/Develop/pinephone-manjaro-sysroot/
#VOLUME /root/Develop/pinephone-manjaro-sysroot

RUN curl https://archlinuxarm.org/builder/xtools/x-tools8.tar.xz | tar Jx -C /root/Develop

COPY pinephone_toolchain.cmake /root/Develop

WORKDIR build
#CMD QEMU_LD_PREFIX=~/Develop/pinephone-manjaro-sysroot cmake -GNinja -DCMAKE_TOOLCHAIN_FILE=~/Develop/pinephone_toolchain.cmake ~/kde/src/kwin -DKWIN_HOST_TOOLING=~/kde/build/kwin -DCMAKE_CXX_COMPILER_LAUNCHER=ccache -DBUILD_TESTING:BOOL=OFF
