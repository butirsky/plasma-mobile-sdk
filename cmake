#!/bin/sh
# CMake wrapper for use in Qt Creator, etc.

podman start sdk_toolchain_1 >/dev/null
exec podman exec -w "$PWD" -it sdk_toolchain_1 cmake $@
# otherwise, Qt Creator doesn't recognize cmake for some reason..
#podman stop sdk_toolchain_1 >/dev/null
