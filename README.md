[TOC]

### Prerequisites
If `podman-compose` is packetized for your distro, the easiest would be to install just that:
- On Arch:  
  ```
  pacman -S podman-compose
  ```
Otherwise:
- Container Engine:
  - [Podman](https://podman.io/) (prefferable, [Installation](https://podman.io/getting-started/installation))  
  or
  - [Docker](https://docs.docker.com/engine/) ([Installation](https://docs.docker.com/engine/install/))

- [Compose Spec](https://compose-spec.io/) tool of your choose:
  - [Podman Compose](https://github.com/containers/podman-compose) ([Installation](https://github.com/containers/podman-compose#installation)):
	```
	pip3 install podman-compose
	```
  - [Docker Compose V1](https://github.com/docker/compose) ([Installation](https://docs.docker.com/compose/install/#install-compose))
  - [Docker Compose V2](https://docs.docker.com/compose/cli-command/) ([Installation](https://docs.docker.com/compose/cli-command/#install-on-linux))

QEMU user mode emulation binaries:
```
$ sudo apt install -y qemu-user-static binfmt-support
# or
$ sudo dnf install qemu-user-static qemu-user-binfmt
```
### Installation
- Clone this repo:
	```sh
	git clone https://invent.kde.org/butirsky/plasma-mobile-sdk.git && cd plasma-mobile-sdk
	```
- In the repo directory, run:
	```sh
	podman-compose pull
	podman-compose up --no-build sysroot
	kdedir=~/kde podman-compose up --no-build --no-start toolchain
	kdedir=~/kde podman-compose run --no-deps --rm toolchain
	```
	Replace `~/kde` with a common directory where all your KDE sources and (future) build trees reside at.

	If you prefer Docker, replace `podman` in the commands above with `docker`. For Docker Compose V2, use `docker compose` instead of `docker-compose`. You'll also have to do the same in `cmake` file of this repo.

The commands will create and start two containers: _sdk_sysroot_1_ and _sdk_toolchain_1_. You can adjust them later for your needs.

Installation process comes with an Intro. You will be invited to cross-compile KWin for arm64, as a proof-of-concept.

### Usage
Set `cmake` wrapper in this repo as a cmake executable for Qt Creator or IDE of your choose.

To do that:
- Add a new CMake with our `cmake` wrapper:
  - Tools / Options / Kits / CMake / Add to add a CMake
  - Set path to the `cmake` wrapper in "Path:" field there, press Apply
  - There shouldn't be any error sign along our CMake in Name column
- Add a new Kit and set our new CMake there:
  - Tools / Options / Kits / Kits / Add to add a Kit
  - select our CMake in "CMake Tool:" list
  - Optionally, change "Device Type:" to Generic Linux Device, add and select your device in "Device:" list, e.g., PinePhone.  
    That should make it easier to deploy built binaries to a remote device.

Now you should be able to configure, build locally as usual, and also deploy and run your projects on a remote ARM device.  
Have fun!


### Feedback
If something doesn't work for you, or you just wanted to say "Thank you" - please open an issue.  
I'm waiting for your feedback! :)

### Credits
Many thanks to:
- [Han Young](@hanyoung), without his "Cross-compile to PinePhone" [blog posts](https://www.hanyoung.uk/blog/cross-compile-to-pinephone-part-one), this work wouldn't be possible
- [Dan Johansen](@strit), for his [Manjaro ARM x86_64 docker image](https://hub.docker.com/r/manjaroarm/manjaro-aarch64-base) - which is a base for Sysroot
- [Aleix Pol](@apol), for opening an [SDK task](https://invent.kde.org/teams/plasma-mobile/issues/-/issues/74) and overall inspiration
- You, for your interest :)
